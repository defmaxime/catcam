const net = require('net');
const config = require('./config.json')
const PIPE_NAME = 'slobs';
const PIPE_PATH = '\\\\.\\pipe\\';
var last_id = 1

const clientRPC = net.connect(PIPE_PATH + PIPE_NAME, () => {
  console.log('connected to server!');
});

clientRPC.on('end', () => {
  console.log('disconnected from server');
});


const clientId = 'koesotiepim02e2gg6jqvyjresch8r';
const clientSecret = 'pe9cxxun2sllftjyshxb40fp3hkbai';

function getFolderByNameFromNodesItemList(incomming, item_to_find) {
  // if (incomming.result == null) {
  //   return null;
  // }
  // console.log('nodes received :');
  return incomming.filter(current => {
    // console.log('Name : '+current.name);
    return current.name == item_to_find
  })[0];
}

async function getCurrentSceneContent(item_to_find) {
  var data = { jsonrpc: "2.0", id: last_id, method: "activeScene", params: { resource: "ScenesService" } }
  console.log("getCurrentSceneContent on "+item_to_find)

  const returnPromise = new Promise((resolve) => {
    clientRPC.on('data', (data) => {
      try {
        let incomming = JSON.parse(data.toString())
        let result_folder_item = getFolderByNameFromNodesItemList(incomming.result.nodes, item_to_find);
        if (result_folder_item == null) {
          console.log('No "Webcam" in this scene');
          clientRPC.removeAllListeners('data');
          resolve(null);
        } else {
          let children_item = getItemFromIDs(result_folder_item.childrenIds, incomming.result.nodes)
          console.log("children fetched")
          clientRPC.removeAllListeners('data');
          resolve(children_item);
        }
      } catch (error) {
        console.log(error)
      }
    });
  })


  clientRPC.write(JSON.stringify(data))
  return returnPromise;
}

function setVisibility(child) {
  console.log('set visibility to ' + !child.visible)
  var data = { jsonrpc: "2.0", id: last_id, method: "setVisibility", params: { resource: child.resourceId, args: [!child.visible] } }
  clientRPC.write(JSON.stringify(data))
}

async function toggleItemVisibility(itemName) {
  console.log("toggleItemVisibility on "+itemName+" begin")
  let children = await getCurrentSceneContent(itemName);

  if(children == null){
    console.log('no children found')
    return null;
  }

  children.forEach((child, index)=>{
    setTimeout(() => {
      setVisibility(child);
      last_id++;
      }, index * "1000")
  })
}

function getItemFromIDs(children_IDs, all_items) {
  let children_items = Array();
  all_items.forEach(current_item => {
    children_IDs.forEach(child_ID => {
      if (current_item.id == child_ID) {
        console.log("child found : Name : "+current_item.name)
        if (current_item.sceneNodeType == 'folder') {
          console.log("folder found : Name : "+current_item.name)
          let sub_folder_children_items = getFolderByNameFromNodesItemList(all_items,current_item.name)
          let sub_children_ID = getItemFromIDs(sub_folder_children_items.childrenIds,all_items)
          console.log(sub_folder_children_items)
        } else {
          children_items.push(current_item)
        }
      }
    })
  })
  return children_items;
}

const tmi = require('tmi.js');

// Setup connection configurations
// These include the channel, username and password
const client = new tmi.Client({
  options: { debug: true, messagesLogLevel: "info" },
  connection: {
    reconnect: true,
    secure: true
  },

  // Lack of the identity tags makes the bot anonymous and able to fetch messages from the channel
  // for reading, supervision, spying, or viewing purposes only
  identity: {
    username: 'gp762nuuoqcoxypju8c569th9wz7q5',
    password: 'oauth:iobp2mojnm9a3jph503rd4c9cp85iw'
  },
  channels: ['maskim64']
});

// Connect to the channel specified using the setings found in the configurations
// Any error found shall be logged out in the console
client.connect().catch(console.error);

// We shall pass the parameters which shall be required
client.on('message', (channel, tags, message, self) => {
  // Lack of this statement or it's inverse (!self) will make it in active
  if (self) return;
  switch (message.toLowerCase()) {
    // Use 'tags' to obtain the username of the one who has keyed in a certain input
    // 'channel' shall be used to specify the channel name in which the message is going to be displayed
    //For one to send a message in a channel, you specify the channel name, then the message
    // We shall use backticks when using tags to support template interpolation in JavaScript

    // In case the message in lowercase is equal to the string 'commands', send the sender of that message some of the common commands

    case 'commands':
      client.say(channel, `@${tags.username}, available commands are:
          Commands Help Greetings Hi !Website !Name
         
          For more help just type "Help"
          `);
      break;

    // In case the message in lowercase is equal to the string '!website', send the sender of that message your personal website
    case '!website':
      client.say(channel, `@${tags.username}, my website is www.section.io!`);
      break;

    // In case the message in lowercase is equal to the string 'greetings', send the sender of that message 'Hello @Username, what's up?!'
    case 'greetings':
      client.say(channel, `Hello @${tags.username}, what's up?!`);
      break;

    // In case the message in lowercase is equal to the string 'hi', send the sender of that message 'Username, hola'
    case 'hi':
      client.say(channel, `${tags.username}, hola!`);
      break;

    // In case the message in lowercase is equal to the string '!name', send the sender of that message the name of the chatbot
    case '!name':
      client.say(channel, `Hello @${tags.username}, my name is ChatBot! Type "help" to continue...`);
      break;

    case '!catcam':
      toggleItemVisibility(config.catcam.name)
      // toggleCatCam()
      break;
    case '!webcam':
      toggleItemVisibility(config.webcam.name)
      // toggleWebcam()
      break;

    // In case the message in lowercase is equal to the string 'help', send the sender of that message all the available help and commands
    case 'help':
      client.say(channel, `${tags.username}, Use the following commands to get quick help:
          -> Commands: Get Commands || 
          Help: Get Help || 
          Greetings: Get Greetings || 
          Hi: Get "Hola" || 
          !Website: Get my website || 
          !Name: Get my name || 
          !Upvote first_name second_name: Upvote user first_name second_name ||  Upvote first_name second_name: Upvote user first_name second_name || 
          !Cheer first_name second_name: Cheer first_name second_name || Cheers first_name second_name: Cheer first_name second_name --

          For more help just ping me up!
          `);
      break;


    // In case the message in lowercase is none of the above, check whether it is equal to '!upvote' or '!cheers'
    // these are used to  like certain users' messages or celebrate them due to an achievement

    default:
      // We shall convert the message into a string in which we shall check for its first word
      // and use the others for output
      let mymessage = message.toString();

      // // We shall split the input message and check the string before the space if it is equal to '!upvote' or 'upvote'
      // if ((mymessage.split(' ')[0]).toLowerCase() === '!upvote' || 'upvote') {

      //     // You can add some emojis which will appear in the chat using their emoji names
      //     // For example "PopCorn" or "TwitchLit" (fire emoji)
      //     // We shall then take the first and second strings after the space and display them together with the username
      //     // This shall output 'fireEmoji first_name second_name fireEmoji you have been UPVOTED by USERNAME'
      //     client.say(channel, `TwitchLit @${(mymessage.split(' ')[1] + '_' + mymessage.split(' ')[2])} TwitchLit  you have been UPVOTED by ${ tags.username }`);


      //     // We shall check if it is !cheer or cheers
      //     // If so, we shall display beer emojis (HSCheers) and messages
      //     // The bots output shall be 'beerEmoji first_name second_name beerEmoji you have been UPVOTED by USERNAME'
      // } else if ((mymessage.split(' ')[0]).toLowerCase() === '!cheer' || 'cheers') {
      //     console.log(`HSCheers @${(mymessage.split(' ')[1] + '_' + mymessage.split(' ')[2])} HSCheers you have been UPVOTED by ${ tags.username }`);
      // }
      break;
  }
});